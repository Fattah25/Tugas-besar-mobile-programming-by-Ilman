package com.example.movieappilman.di

import com.example.movieappilman.core.domain.usecase.movie.MovieInteractor
import com.example.movieappilman.core.domain.usecase.movie.MovieUseCase
import com.example.movieappilman.ui.movie.MovieViewModel
import com.example.movieappilman.ui.tvshow.TvShowViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val useCaseModule = module {
    factory<MovieUseCase> { MovieInteractor(get()) }

}

val viewModelModule = module {
    viewModel { MovieViewModel(get()) }
    viewModel { TvShowViewModel(get()) }
}
